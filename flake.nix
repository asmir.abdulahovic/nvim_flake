{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    nixpkgs,
  }: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
  in {
    nvim = pkgs.mkShell {
      nativeBuildInputs = [
        pkgs.alejandra
        pkgs.ccls
        pkgs.luaformatter
        pkgs.nixd
        pkgs.pyright
        pkgs.rust-analyzer
        pkgs.sumneko-lua-language-server
        pkgs.svls
        pkgs.texlab
        pkgs.tree-sitter
        pkgs.verible
        pkgs.zls
        (import ./nvim-lsp.nix {inherit pkgs;})
      ];
    };
  };
}
