{pkgs ? import <nixpkgs> {}}: let
  neovim = pkgs.neovim.override {
    configure = {
      customRC = ''
        lua <<EOF
        ${luaRc}
        EOF
      '';

      packages.myPlugins.start = with pkgs.vimPlugins; [
        (nvim-treesitter.withPlugins (parsers: [
          parsers.c
          parsers.cpp
	  parsers.go
          parsers.json
          parsers.latex
          parsers.lua
          parsers.nix
          parsers.python
          parsers.query
          parsers.rust
          parsers.verilog
          parsers.vimdoc
          parsers.zig
        ]))
        cmp-buffer
        cmp-cmdline
        cmp-nvim-lsp
        cmp-nvim-ultisnips
        cmp-path
        colorizer
        fugitive
        fzf-lua
	gopls
        gruvbox
        nvim-cmp
        nvim-lspconfig
        nvim-ts-rainbow
        repeat
        targets-vim
        UltiSnips
        vim-addon-nix
        vim-signify
        vim-slime
        vim-snippets
        zig-vim
      ];
    };
  };

  # lua
  luaRc = builtins.readFile ./vimrc.lua;
in
  pkgs.runCommand "nvim-lsp" {} ''
    mkdir -p $out/bin
    ln -s ${neovim}/bin/nvim $out/bin/vim
  ''
